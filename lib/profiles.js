'use strict';

var fs = require('fs');
var utils = require('../crawler_engine/lib/utils');
var es = require('../crawler_engine/lib/connectors/redilastic').core;
var _ = require('lodash');
var async = require('async');
var request = require('request');
var cheerio = require('cheerio');
var profiles = {};
var urlsQueue = require('double-ended-queue'),
    urls = new urlsQueue([]);

var dirBase = 'http://www.linkedin.com',
    dirSuff = '/directory/people',
    dirSource = __dirname + '/../data/initial.json';

profiles.bootup = function (program, err) {

    var redis = program.redis;

    var dirChar = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26'];
    dirChar.forEach(function (suff) {
        urls.push(dirBase + dirSuff + '-' + suff);
    });

    redis.unref();

    var multi = redis.multi();
    for (var i = 0; i < urls.length; i++) {
        multi.rpush('liqueue', urls[i]);
    }
    multi.exec(function (errors, results) {});

    return err = 0;

};

profiles.ids = function (program, err) {

    var redis = program.redis;

    var proxies = utils.getLatestProxies(program);

    var escfg = {
        "work": {
            "index": "work-queue-2014-06",
            "type": "linkedin"
        },
        "connect": {
            "hosts": [
                "http://127.0.0.1:9200"
            ],
            "requestTimeout": "60000",
            "log": [],
            "maxRetries": 1,
            "maxSockets": 10
        }
    };

    if (program.mode == 'production') {
        escfg.connect.hosts = [
            "http://user:secret@10.10.100.93:42193",
            "http://user:secret@10.10.100.94:42193"
        ]
    };

    var connector = new es(escfg);

    async.whilst(function () {
            return true
        },
        function (more) {

            var url;

            redis.unref();

            redis.rpop('liqueue', function (err, id) {
                if (err) throw err;
                url = id;

                if (program.debug) {console.log('----- id value ' + id)};

                if (id == null || id == 'underfined') {
                    return 'done';
                };

                var proxy = _(proxies).sample();
                var cfg = utils.setProxyType({
                    url: url
                }, proxy);

                if (program.debug) {console.log('++++++++++++>>>>>>>>>>>>>>>>>', cfg)};

                request(cfg, null, function (err, resp, html) {

                    var thishtml = html;

                    var $ = cheerio.load(html);
                    var dirLink = $('#body .directory li a');

                    dirLink.each(function (idx, link) {

                        var dirpath = $(link).attr('href');

                        if (dirpath.search('/directory') != -1) {
                            redis.rpush('liqueue', dirBase + dirpath);

                            if (program.debug) {
                                console.log('get another dir page - ' + dirBase + dirpath)
                            };

                        } else if (dirpath.search('pub/dir') != -1) {

                            var cfg = utils.setProxyType({
                                url: dirBase + dirpath
                            }, proxy);

                            if (program.debug) {console.log('++++++++++++>>>>>>>>>>>>>>>>>', cfg)};

                            request(cfg, null, function (merr, mresp, mhtml) {

                                var m$ = cheerio.load(mhtml);

                                var refLink = m$('#refine-search .content .facets li a');

                                refLink.each(function (idx, rlink) {

                                    if (program.debug) {console.log ('---------- country link ' + m$(rlink).attr('title'))};

                                    if (m$(rlink).attr('title')) {

                                        var dirpath = m$(rlink).attr('href');

                                        if (dirpath.split('.')[0] != 'http://us') {

                                            if (program.debug) {console.log('---------- country link ' + dirpath)};
                                            redis.rpush('liqueue', dirBase + dirpath);

                                            if (program.debug) {
                                                console.log('adding to work index profile link from country sub_page - ' + dirpath)
                                            };
                                        }
                                    }
                                });

                                var mpLink = m$('#result-set .vcard h2 a');

                                mpLink.each(function (idx, mlink) {

                                    var dirpath = m$(mlink).attr('href');
                                    var name = m$(mlink).attr('title').trim();
                                    connector.putNext(dirpath.split('linkedin.com')[1], {
                                        link: dirpath,
                                        name: name,
                                        timestamp: utils.dateToElastic(new Date())
                                    }, function (err) {});

                                    if (program.debug) {
                                        console.log('adding to work index profile link from sub_page - ' + dirpath)
                                    };

                                });
                            });
                        } else {
                            var name = $(link).text().trim();
                            connector.putNext(dirpath, {
                                link: dirBase + dirpath,
                                name: name,
                                timestamp: utils.dateToElastic(new Date())
                            }, function (err) {});

                            if (program.debug) {
                                console.log('adding to work index profile link - ' + dirBase + dirpath)
                            };
                        }
                    });
                    more();

                    return err = 0;
                });

            });

        },
        function () {

            console.log('parsing li_dir done successfully');

        });

};

module.exports = profiles;