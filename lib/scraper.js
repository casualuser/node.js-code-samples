'use strict';

var async = require('async');

var cheerio = require('cheerio');
var lodash = require('lodash');

var utils = require('../crawler_engine/lib/utils');

var parsers = require('./parser');

exports.LinkedInProfile = function LinkedInProfile() {

    this.process = function (data, api, callback) {

        var url = data.link;

        api.loadUrl(url, null, function (error, body) {

            if (error == 'Error: invalid status code: 999') {
                return callback(new utils.CrawlerError('-------------->>>>>>> error - proxy filtered', 'relog'), null);
            } else if (error) {
                return callback(new utils.CrawlerError('-------------->>>>>>> error - network error', 'network'), null);
            };

            var $ = cheerio.load(body);

            async.parallel([
                    function (_cb) {
                        var signup = $('#pagekey-reg-cold-signup').length;
                        if (signup) {
                            _cb('signup');
                        }
                    },
                    function (_cb) {
                        var v1 = $('#pagekey-nprofile-public-success').length;
                        if (v1) {
                            _cb('v1');
                        }
                    },
                    function (_cb) {
                        var v2 = $('#pagekey-nprofile_v2_public_fs').length;
                        if (v2) {
                            _cb('v2');
                        } else {
                            _cb('none');
                        }
                    }
                ],
                function (page) {
                    if (page == 'signup') {
                        return callback(new utils.CrawlerError('>>>>>>> private public profile found', 'notfound'), null);

                    } else if (page == 'v1') {
                        var output = parsers.pubProfile_v1($, api);
                        output.networks[0].link = url;
                        output.networks[0].id.push(decodeURI(data._id));
                        var uid = body.split("newTrkInfo = '")[1];
                        if (uid) {
                            uid = uid.split(",' +")[0];
                            if (uid) output.networks[0].id.push(uid);
                        };
                        return callback(null, output);

                    } else if (page == 'v2') {
                        var output = parsers.pubProfile_v2($, api);
                        output.networks[0].link = url;
                        output.networks[0].id.push(decodeURI(data._id));
                        var uid = body.split("newTrkInfo = '")[1];
                        if (uid) {
                            uid = uid.split(",' +")[0];
                            if (uid) output.networks[0].id.push(uid);
                        };
                        return callback(null, output);

                    } else {
                        return callback(new utils.CrawlerError('>>>>>> fetched page is not profile or register page', 'notfound'), null);
                    };
                });

        });
    };
};