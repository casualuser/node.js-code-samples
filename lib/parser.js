'use strict';

var moment = require('moment');
var utils = require('../crawler_engine/lib/utils');

var parsers = {};

var network = "linkedin";

function convertDate(date) {
    if (moment(date, 'YYYY', true).isValid()) {
        return date;
    };
    date = moment.unix(moment(date).format('X')).format('YYYYMMDD');
    return date;
};

parsers.pubProfile_v1 = function ($, api) {

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> start parsing profile v1')};

    var $content = $('#content');
    var $profile = $content.find('.profile-header');

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> main data loaded')};

    var givenName = $profile.find('.given-name').text().trim();
    var familyName = $profile.find('.family-name').text().trim();
    var fullName = givenName + ' ' + familyName;
    var title = $profile.find('.headline-title').text().trim();
    var location = $profile.find('.locality').text().trim();

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> plain data loaded')};

    var $browsemap = $('.browsemap .content');
    var $insights = $('.insights-browse-map');

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> browsermap catched')};

    if ($browsemap.length == 1) {
        $browsemap.find('li strong a').each(function () {
            var profileLink = $(this).attr('href').split('?trk=')[0];
            var profileName = $(this).text().trim();
            if (program.debug) {console.log('+=+=+=+=+=+=+=+=+=+', profileLink, profileName)};
            api.putNext(profileLink.split('linkedin.com')[1], {
                link: profileLink,
                name: profileName,
                timestamp: utils.dateToElastic(new Date())
            }, function (err) {});
        });
    };

    if ($insights.length == 1) {
        $insights.find('li h4 a').each(function () {
            var profileLink = $(this).attr('href').split('?trk=')[0];
            var profileName = $(this).text().trim();
            if (program.debug) {console.log('+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=', profileLink, profileName)};
            api.putNext(profileLink.split('linkedin.com')[1], {
                link: profileLink,
                name: profileName,
                timestamp: utils.dateToElastic(new Date())
            }, function (err) {});
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> browsermap parsed')};

    var output = {

            networks: [{
                id: [],
                link: null,
                timestamp: utils.dateToElastic(new Date()),
                network: network
            }],

            src: network,

            fullName: [fullName],

            places: [{
                value: location,
                network: network
            }]
        },
        $data = [];

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> basic output ready')};

    var picture = $content.find('#profile-picture img').attr('src');

    if (picture) {
        var $picture = {
            url: $content.find('#profile-picture img').attr('src'),
            network: network,
            tag: "thumb"
        };

        if (output['pictures']) {
            output['pictures'].push($picture);
        } else {
            output['pictures'] = [];
            output['pictures'].push($picture);
        };
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> picture ready')};

    if (title != '--') {
        output.header = title;
    };

    $content.find('.section').each(function () {
        var section = $(this).attr('id') ? $(this).attr('id').split('profile-')[1] : 'timestamp';
        $data[section] = $(this);
    });

    /*
    $data['additional'] ? $data['additional'].each(function() {
        $(this).find('dd').each(function() {
            output['additional'].push({
        //2do - improve website links parsing for multiply records
                website:        $(this).find('li.website') ? $(this).find('li.website a').attr('href').split('redirect?url=')[1] : '',
                interests:      $(this).find('.interests p') ? $(this).find('.interests p').text().trim().split(', ') : '',
                groups:         ''
            });
        });
    }) : output['additional'] = [];
*/

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> sections catched')};

    if ($data['education']) {
        $data['education'].each(function () {

            $(this).find('.position').each(function () {

                var $education = {
                    network: network,
                    school: $(this).find('h3.summary').text().trim()
                };

                if ($(this).find('h4 .degree').hasClass('degree')) {
                    $education.degree = $(this).find('h4 .degree').text().trim()
                };

                if ($(this).find('.period .dtstart').hasClass('dtstart')) {
                    if (program.debug) {console.log('education start', $(this).find('.period .dtstart').attr('title'))};
                    var date = convertDate($(this).find('.period .dtstart').attr('title'));
                    if (program.debug) {console.log('education start parsed', date)};
                    if (date != 'Invalid date') {
                        $education.start = date;
                    };
                };

                if ($(this).find('.period .dtend').hasClass('dtend')) {
                    if (program.debug) {console.log('education end', $(this).find('.period .dtend').attr('title'))};
                    var date = convertDate($(this).find('.period .dtend').attr('title'));
                    if (program.debug) {console.log('education end parsed', date)};
                    if (date != 'Invalid date') {
                        $education.end = date;
                    };
                };

                if (output['education']) {
                    output['education'].push($education)
                } else {
                    output['education'] = [];
                    output['education'].push($education);
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> education ready')};

    if ($data['experience']) {
        $data['experience'].each(function () {
            $(this).find('.position').each(function () {

                var $experience = {
                    title: $(this).find('h3 span.title').text().trim(),
                    company_li:  $(this).find('h4 a').attr('href') ? $(this).find('h4 a').attr('href').split('?trk=')[0] : '',
                    location:    $(this).find('.location').text().trim(),
                    description: $(this).find('.description').text().trim()
                    network: network
                };

                if ($(this).find('.period .dtstart').hasClass('dtstart')) {
                    if (program.debug) {console.log('exp start', $(this).find('.period .dtstart').attr('title'))};
                    var date = convertDate($(this).find('.period .dtstart').attr('title'));
                    if (program.debug) {console.log('exp start parsed', date)};
                    if (date != 'Invalid date') {
                        $experience.start = date;
                    };
                };

                if ($(this).find('.period .dtend').hasClass('dtend')) {
                    if (program.debug) {console.log('exp end', $(this).find('.period .dtend').attr('title'))};
                    var date = convertDate($(this).find('.period .dtend').attr('title'));
                    if (program.debug) {console.log('exp end parsed', date)};
                    if (date != 'Invalid date') {
                        $experience.end = date;
                    };
                };

                if ($(this).find('h4 .summary').text().trim()) {
                    $experience.company = $(this).find('h4 .summary').text().trim();
                };

                if (output['experience']) {
                    output['experience'].push($experience)
                } else {
                    output['experience'] = [];
                    output['experience'].push($experience);
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> exp ready')};

    if ($data['languages']) {
        $data['languages'].each(function () {
            $(this).find('li.language').each(function () {
                if (output['languages']) {
                    output['languages'].push($(this).find('h3').text().trim());
                } else {
                    output['languages'] = [];
                    output['languages'].push($(this).find('h3').text().trim());
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> langs ready')};

    $data['summary'] ? $data['summary'].find('.content').each(function() {
        output['summary'] = $(this).find('.summary').text().trim()
    }) : output['summary'] = '';

    $data['contact'] ? $data['contact'].each(function() {
        $(this).find('.interested li').each(function() {
        output['contact'].push(
            $(this).text().trim()
            );
        });
    }) : output['contact'] = [];

    $data['certifications'] ? $data['certifications'].each(function() {
        $(this).find('li.certification').each(function() {
        output['certifications'].push({
            title:       $(this).find('h3').text().trim(),
            company:     $(this).find('.org').text().trim(),
            date:        $(this).find('.dtstart').text().trim()
            });
        });
    }) : output['certifications'] = [];

    if ($data['skills']) {
        $data['skills'].each(function () {
            $(this).find('li.show-bean').each(function () {

                var $skill = {
                    value: $(this).find('.jellybean').text().trim(),
                    network: network
                };

                if (output['skills']) {
                    output['skills'].push($skill);
                } else {
                    output['skills'] = [];
                    output['skills'].push($skill);
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> skills ready')};

    $data['patents'] ? $data['patents'].each(function() {
        $(this).find('li.patent').each(function() {
            output['patents'].push({
                title: $(this).find('h3 a').text().trim(),
                link: $(this).find('h3 a').attr('href') ? $(this).find('h3 a').attr('href').split('redirect?url=')[1] : '',
                org: $(this).find('.specifics li').first().find('.org').text().trim(),
                number: $(this).find('.specifics li').last().text().trim(),
                date: $(this).find('.specifics .dtstart').text().trim(),
                attribution: $(this).find('.attribution').contents().text().trim(), //2Do => check profiles links for owner
                summary: $(this).find('.summary p').text().trim(),
            });
        });
    }) : output['patents'] = [];

    $data['publications'] ? $data['publications'].each(function() {
        $(this).find('li.publication').each(function() {
            output['publications'].push({
                title: $(this).find('h3 cite').text().trim(),
                link: $(this).find('h3 a').attr('href') ? $(this).find('h3 a').attr('href').split('redirect?url=')[1] : '',
                org: $(this).find('.specifics li').first().text().trim(),
                date: $(this).find('.specifics .dtstart').text().trim(),
                attribution: $(this).find('.attribution').contents().text().trim(), //2Do => check profiles links for owner
                summary: $(this).find('.summary p').text().trim()
            });
        });
    }) : output['publications'] = [];

    $data['projects'] ? $data['projects'].each(function() {
        $(this).find('li.publication').each(function() {
            output['projects'].push({
                title: $(this).find('h3 a').text().trim(),
                link: $(this).find('h3 a').attr('href') ? $(this).find('h3 a').attr('href').split('redirect?url=')[1] : '',
                date: $(this).find('.specifics li').text().trim(),
                attribution: $(this).find('.attribution').contents().text().trim(), //2Do => check profiles links for owner
                summary: $(this).find('p').text().trim()
            });
        });
    }) : output['projects'] = [];

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> end parsing profile v1')};

    return output;
};

parsers.pubProfile_v2 = function ($, api) {

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> start parsing profile v2')};

    var $content = $('#profile');
    var $profile = $content.find('#top-card');

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> main data loaded')};

    var fullName = $profile.find('.full-name').text().trim();
    var title = $profile.find('#headline p').text().trim();
    var location = $profile.find('.locality').text().trim();
    var industry = $profile.find('.industry').text().trim();

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> plain data loaded')};

    var $insights = $('.insights-browse-map');

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> browsermap catched')};

    if ($insights.length == 1) {
        $insights.find('li h4 a').each(function () {
            var profileLink = $(this).attr('href').split('?trk=')[0];
            var profileName = $(this).text().trim();
            if (program.debug) {console.log('+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=', profileLink, profileName)};
            api.putNext(profileLink.split('linkedin.com')[1], {
                link: profileLink,
                name: profileName,
                timestamp: utils.dateToElastic(new Date())
            }, function (err) {});
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> browsermap parsed')};

    var output = {

            networks: [{
                id: [],
                link: null,
                timestamp: utils.dateToElastic(new Date()),
                network: network
            }],

            src: network,

            fullName: [fullName],

            places: [{
                value: location,
                network: network
            }]

        },
        $data = [];

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> basic output ready')};

    var picture = $profile.find('.profile-picture img').attr('src');

    if (picture) {
        var $picture = {
            url: $content.find('.profile-picture img').attr('src'),
            network: network,
            tag: "thumb"
        };
        if (output['pictures']) {
            output['pictures'].push($picture);
        } else {
            output['pictures'] = [];
            output['pictures'].push($picture);
        };
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> picture ready')};

    if (title != '--') {
        output.header = title;
    };

    $content.find('.background-section').each(function () {
        var section = $(this).children().attr('id') ? $(this).children().attr('id').split('-')[1] : 'timestamp';
        $data[section] = $(this).children();
    });

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> sections catched')};

    if ($data['education']) {
        $data['education'].each(function () {

            $(this).find('.section-item').each(function () {

                var $education = {
                    network: network,
                };

                if ($(this).find('h4.summary a')) {
                    $education.school = $(this).find('h4.summary a').text().trim();
                } else
                    $education.school = $(this).find('h4.summary').text().trim();

                if ($(this).find('h5 .degree').hasClass('degree')) {
                    $education.degree = $(this).find('h5 .degree').text().trim() + ' ' + $(this).find('h5 .major').text().trim();
                };

                if ($(this).find('.education-date').children().first().text()) {
                    var date = convertDate($(this).find('.education-date').children().first().text().trim());
                    if (date != 'Invalid date') {
                        $education.start = date;
                    };
                };

                if ($(this).find('.education-date').children().last().text()) {
                    var date = convertDate($(this).find('.education-date').children().last().text().trim());
                    if (date != 'Invalid date') {
                        $education.end = date;
                    };
                };

                if (output['education']) {
                    output['education'].push($education)
                } else {
                    output['education'] = [];
                    output['education'].push($education);
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> education ready')};

    if ($data['experience']) {
        $data['experience'].each(function () {
            $(this).find('.section-item').each(function () {

                var $experience = {
                    title: $(this).find('h4').text().trim(),
                    network: network
                };

                if ($(this).find('.experience-date-locale').children().first().text()) {
                    var date = convertDate($(this).find('.experience-date-locale').children().first().text().trim());
                    if (date != 'Invalid date') {
                        $experience.start = date;
                    };
                };

                if ($(this).find('.experience-date-locale').children().last().text()) {
                    var date = convertDate($(this).find('.experience-date-locale').children().last().text().trim());
                    if (date != 'Invalid date') {
                        $experience.end = date;
                    };
                };

                if ($(this).find('h4').next().find('a').text()) {
                    $experience.company = $(this).find('h4').next().find('a').text().trim();
                } else {
                    $experience.company = $(this).find('h4').next().text().trim();
                };

                if (output['experience']) {
                    output['experience'].push($experience);
                } else {
                    output['experience'] = [];
                    output['experience'].push($experience);
                };
            });
        })
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> exp ready')};

    if ($data['languages']) {
        $data['languages'].each(function () {
            $(this).find('section-item ').each(function () {
                if (output['languages']) {
                    output['languages'].push($(this).find('h4').text().trim());
                } else {
                    output['languages'] = [];
                    output['languages'].push($(this).find('h4').text().trim());
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> langs ready')};

    if ($data['skills']) {
        $data['skills'].each(function () {
            $(this).find('li.endorse-item ').each(function () {

                var $skill = {
                    value: $(this).find('.endorse-item-name-text').text().trim(),
                    network: network
                };

                if (output['skills']) {
                    output['skills'].push($skill);
                } else {
                    output['skills'] = [];
                    output['skills'].push($skill);
                };
            });
        });
    };

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> skills ready')};

    if (program.debug) {console.log('---++++++++++++---->>>>>>>>> end parsing profile v2')};

    return output;
};

module.exports = parsers;