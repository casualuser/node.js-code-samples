#!/usr/bin/env node

'use strict';

var profiles = require('../lib/profiles');

var numCPUs = require('os').cpus().length;
var cluster = require('cluster');

var program = require('commander');
var domain = require('domain');

var hiredis = require("redis");

program
    .version('1.0.1')
    .option('-f, --fork <num>', 'number of parallel processes to run', 10)
    .option('-d, --debug', 'debug', false)
    .option('-i, --init', 'init', false)
    .option('-m, --mode [mode]', 'mode', 'local')
    .option('-p, --proxy [proxy]', 'proxy', 'socks-li.json')
    .parse(process.argv);

program.redis = hiredis.createClient('/tmp/redis.sock', null);

process.maxTickDepth = 10000;

if (cluster.isMaster) {

    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    };

    cluster.on('disconnect', function (worker) {
        console.error('disconnect! ----- ' + worker.process.pid);
        if (program.debug) {
            console.error(require('util').inspect(worker, false, null));
            console.error(require('util').inspect(redis, false, null));
        };
        cluster.fork();
    });

    cluster.on('exit', function (worker, code, signal) {
        console.error('worker ' + worker.process.pid + ' died');
    });

    cluster.on('online', function (worker) {
        if (program.debug) {console.log("------- Yay, the worker responded after it was forked")};
    });

    program.redis.on("error", function (err) {
        console.error("Redis emitted error: ", err.message);
        console.error(err.stack);
    });

    if (program.init) {
        profiles.bootup(program, function (err) {
            console.error("Redis emitted error: ", err.message);
            console.error(err.stack);
        });
    };

} else {

    profiles.ids(program, function (err) {
        console.error("Redis emitted error: ", err.message);
        console.error(err.stack);
    });
    //		console.log("Yay, the worker responded after it was forked");
}