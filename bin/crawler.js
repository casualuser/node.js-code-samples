#!/usr/bin/env node

'use strict';

var crawler = require('../crawler_engine/lib/crawler').ScrapeEngine;
var scraper = require('../lib/scraper').LinkedInProfile;

//var li_uId = ['in/jasonhoffman', 'in/williamhgates', 'in/marissamayer', 'in/jeffweiner08','in/barackobama'];
//li_uId.push('pub/aleksei-tcelishchev/12/69a/330');

var client = new crawler({
    //	input: new connector.array.inputSimple(li_uId),
    /*	input: new connector.redilastic.core({
		"work":  {
			"index": 	"work",
			"type": 	"linkedin"
		},
		"connect": {
			"hosts": ["http://localhost:9200"],
			"apiVersion": "0.90",
			"requestTimeout": "5000",
			"log": []
		}
	}),*/
    // output: new connector.filesystem.outputDirectory('debug'),
    name: 'linkedin',
    validator: 'linkedin',
    scraper: scraper
});