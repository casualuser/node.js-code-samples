#!/usr/bin/env node

'use strict';

var numCPUs = require('os').cpus().length;
var cluster = require('cluster');

cluster.setupMaster({
    exec: "./bin/crawler.js",
    args: ["-s", "127.0.0.1:25052"]
});

for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
};

cluster.on('exit', function (worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
});