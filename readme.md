# LinkedIn scraper

new version for updated crawler_engine

## Installation

change location to smth like '/home/user/' and run this commands:

`````
git clone git@github.com:casualuser/linkedin.git ./linkedin
cd linkedin/
git fetch
git checkout release
git pull
git submodule update --init --recursive
npm install
cd crawler_engine/
npm install
`````

## Usage

Run from the project directory:

to start server instance of crawler_engine (if there is no any running yet or another instance required) run this from './linkedin' folder
`````
node ./crawler_engine/bin/server.js -c /home/casualuser/linkedin/crawler_engine/config-li.json -m -d -p 25052
`````

then from same './linkedin' folder run next command to start crawler process itself:

`````
node ./bin/run.js -s 127.0.0.1:25052 -v -d -c /home/casualuser/linkedin/crawler_engine/config-li.json >> ./linkedin.log 2>&1
`````

omit using -v & -d flags if output info to console is not required